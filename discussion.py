# lists, Disctionaries, Functions, Classes

# Python has several structures to store collections or multiple items inside a single variable
# Lists, Dictionaries, Tuples, Sets

# [Section] Lists
# Lists are similar to arrays in JS.
names = [ "John", "Paul", "George", "Ringo" ] # string list
programs = [ 'developer career', 'pi-shape', 'short courses' ] # string list
durations = [ 260, 180, 20 ] # number list
truth_values = [ True, False, True, True, False ] # Boolean List

# having multiple data types inside a list is not recommended because it could lead to confusion as to what is the relationship of the elements
sample_list = [ "Apple", 3, False, 'Potato', 4, True ]
print(names)
print(sample_list)

# getting the size of the list
# len() is similar to .length property of arrays in JS
print(len(programs))

# accessing of values
# lists can be accessed by providing the index number of the element
# accessing the first element
print(programs[0])
# accesing the second element
print(durations[1])
# last item using negative index
print(programs[-1])
# using negative index
print(names[-2])

# printing of the whole list
print(programs)

# accessing a range of index values
# list_name[start_index : end_index]
print(programs[0:2]) # index 0 - index 1
# Miniactivity
# 1. create a list of names with 5 students
# 2. create another list of grades for the five 5 students
# 3. using a loop, iterate through the lists printing the following format: "The grade of <student> is <grade>."
# 6:58 pm IST; please send the screenshot of the output in our batch google chat
students = [ "Mary", "Matthew", "Tom", "Anne", "Thomas" ]
grades = [ 100, 85, 88, 90, 75 ]

count = 0
while count < len(students):
	print(f"The grade of {students[count]} is {grades[count]}.")
	count += 1

# [Section] List Manipulation
# Updating the lists
print(f"Current Value: {programs[2]}")
programs[2] = "Short Courses"
print(f"Updated Value: {programs[2]}")
# Lists are immutable

# Methods - can be used to manipulate the elements 
# adding of elements
programs.append('global')
print(programs)

# Deleting of items
# add items to be deleted
durations.append(360)
print(durations)

# Delete the last item in the list
del durations[-1]
print(durations)

# membership checks
# "in" keyword checks if the element is in the list
# returns the boolean value
print(20 in durations)
print(500 in durations) # False

# sorting
# sort() - sorts the list alphanumerically in an ascending order, by default
names.sort()
print(names)

# emptying the list
# clear() - empties the contents of the list
test_list = [ 1, 2, 3, 4, 5 ]
print(test_list)
test_list.clear()
print(test_list)

# Deleting the test_list deletes the whole list
# Technically the test_list is no longer exist. It shows name error

# [Section] Dictionaries
# Are used to store data in key:value pairs. This is similar to objects in JS
# They are ordered, changeable, and does not allow duplicates
# The difference between JS and python is here, they have defined order that cannot be changed
# in JS, they are alphabetically arranged
# To create a dictionary, the {} is used and key-value are denoted with key:value

person1 = {
	"name": "Brandon",
	"age": 28,
	"occupation": "student",
	"isEnrolled": True,
	"subjects": [ "Python", "SQL", "Graph" ]
}
print(person1)

# Size of the dictionary - denoted by the number of key-calue pairs in the dictionary
print(len(person1))

# accessing the keys in the dictionary
# to get the items in the dictionary, we have to use the syntax: dictionary["key"]
print(person1["name"])
# print(person1.name) - dict has no attribute 'name'

# get all keys of a dictionary
print(person1.keys())
print("---------------------------")
# get akk values of a dictionary
print(person1.values())
print("---------------------------")

# get all items (key-value pairs) of a dictionary
print(person1.items())
print("---------------------------")

# adding key - value pair
person1["nationality"] = "Filipino"
person1.update({"fav_food" : "Sinigang"})
print(person1)
print("-----------------")

# delete key-value pair
person1.pop("fav_food")
del person1["nationality"]
print(person1)
print("-------------------")

# ----------------- MiniActivity ---------
# 1. Create a car dictionary with the following keys:   # brand, model, year_of_make, color
# 2. Print the following statement based on the details of the car "I own a <brand> <model> and ut was made in <year_of_make>"

car = {
	"brand": "ford",
	"model": "vintage",
	"year_of_make": "2009"
}

print(f'I own a {car["brand"]} of {car["model"]} model which was made in the {car["year_of_make"]}')

# clear() empties the dictionary
# Adding a dictionary to be cleared
person2 = {
	"name" : "John",
	"age" : 18
}
print(person2)
person2.clear()
print(person2)

# Nested dictionaries
classroom = {
	"student1" : person1,
	"student2" : person2
}
print(classroom)
print("---------------------------------")

# [Section] Functions
# functions are blocks of code that runs when called 
# can be used to get inputs, process them, and return outputs
# SYNTAX
# def <function_name>():
#			codes_to_be_executed
def my_greeting():
	print('Hello!')
my_greeting()

# Parameters in a function
def greet_user(user):
	print(f"Hello, {user}!")
greet_user("Madara")
greet_user("Sakura")

# "return" keyword allows the function to return values
def addition(num1, num2):
	return num1 + num2
total = addition(5,10)
print(total)

# [Section] Lambda Function
# a small, anonymous function that can be used for callbacks
# it is just like any normal python function, except that it has not name when defined and it contains in one line of code
# a lambda function can take any number of arguments, but can only have one expression
# variable_name = lambda <parameters> : expression

greeting = lambda person: f'Hello {person}'
print(greeting("Tom"))
print(greeting("Anthony"))

mult = lambda a, b : a * b
print(mult(2,3))
print(mult(6,99))
product = mult(6, 99)
print(product)

# [Section] Classes
# blueprint to describe the concept of objects
# each object has characteristics (properties) and behaviors (methods)
class Car():
	# def __init__() - used to create constructor for the properties in a class
    # whenever we define properties and methods in python classes, we always have to supply "self" as parameter
	def __init__(self, brand, model, year_of_make):
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make
		
        # other properties
		self.fuel = "Gasoline"
		self.fuel_level = 0
		
    # methods
	def fill_fuel(self):
		print(f"Current fuel level: {self.fuel_level}")
		print('filling up the fuel tank')
		self.fuel_level = 100
		print(f'New fuel level: {self.fuel_level}')
	def  drive(self, distance):
		print(f'The car has driven {distance} kilometers')
		print(f"The fuel level is now {self.fuel_level - distance}")
		self.fuel_level -= distance
			
new_car = Car("Nissan", "GT-R", 2019)
print(f"My new car is {new_car.brand} {new_car.model} {new_car.year_of_make}")
new_car.fill_fuel()
new_car.drive(50)