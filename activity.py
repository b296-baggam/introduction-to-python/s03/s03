class Camper:
    def __init__(self, name, batch, course_type):
        self.name = name
        self.batch = batch
        self.course_type = course_type

    def career_track(self, course_type):
        print(f"Currently enrolled in the {self.course_type} program")
    
    def info(self, name, batch):
        print(f"My name is {self.name} of batch {self.batch}")

zuitt_camper = Camper("Alan", 100, "python short course")
print(f"Camper Name: {zuitt_camper.name}")
print(f"Camper Batch: {zuitt_camper.batch}")
print(f"Camper Course: {zuitt_camper.course_type}")
zuitt_camper.info(zuitt_camper.name, zuitt_camper.batch)
zuitt_camper.career_track(zuitt_camper.course_type)
        